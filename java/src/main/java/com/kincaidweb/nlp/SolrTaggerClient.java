package com.kincaidweb.nlp;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.GenericSolrRequest;
import org.apache.solr.client.solrj.request.RequestWriter;
import org.apache.solr.client.solrj.response.SimpleSolrResponse;
import org.apache.solr.common.util.NamedList;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SolrTaggerClient {
    private static final Logger logger = LoggerFactory.getLogger(SolrTaggerClient.class);

    private static SolrClient solrClient;
    private static String solrUrl;

    private static void getClient(String solrUrl) {
        solrClient = new HttpSolrClient.Builder(solrUrl)
                .withConnectionTimeout(10000)
                .withSocketTimeout(60000)
                .build();
    }

    /**
     * Tag some text using the SolrTagger
     *
     * @param text the text to tag
     * @return the tagging results
     */
    public static List<Map<String, Object>> solrTag(String text) {
        if (solrClient == null) {
            getClient(solrUrl);
        }

        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setParam("overlaps", "NO_SUB")
                .setParam("tagsLimit", "5000")
                .setParam("fl", "id,name,cui,sui")
                .setParam("wt", "json")
                .setParam("json.nl", "map")
                .setParam("indent", "on")
        ;

        GenericSolrRequest solrRequest = new GenericSolrRequest(SolrRequest.METHOD.POST, "/umls/tag", solrQuery);
        solrRequest.setContentWriter(new RequestWriter.StringPayloadContentWriter(text, "text/plain"));

        List<Map<String, Object>> tags = new ArrayList<>();
        try {
           SimpleSolrResponse result = solrRequest.process(solrClient);

           NamedList namedList = result.getResponse();
           List<NamedList> returnedTags = (List<NamedList>) namedList.asMap(10).get("tags");

           for (NamedList nl : returnedTags) {
               tags.add(nl.asMap(1));
           }
        } catch (SolrServerException | IOException e) {
            logger.error("Error while querying Solr.", e);
        }

        return tags;
    }

    /**
     * Register the methods as Spark user defined functions (UDF) named "solrtag"
     */
    public static void udfRegister(SparkSession spark) {
        solrUrl = spark.sqlContext().getConf("solr.url", "http://localhost:8983");
        getClient(solrUrl);

        List<StructField> tagFields = new ArrayList<>();
        tagFields.add(new StructField("startOffset", DataTypes.IntegerType, false, Metadata.empty()));
        tagFields.add(new StructField("endOffset", DataTypes.IntegerType, false, Metadata.empty()));
        tagFields.add(new StructField("ids", DataTypes.createArrayType(DataTypes.StringType), false, Metadata.empty()));

        StructType tagType = DataTypes.createStructType(tagFields);

        spark.udf().register("solrtag", new UDF1<String, List<Map<String,Object>>>() {
                   @Override
                    public List<Map<String, Object>> call(String rtfString) {
                        return (solrTag(rtfString));
                    }
                }
                , DataTypes.createArrayType(tagType));
    }
}

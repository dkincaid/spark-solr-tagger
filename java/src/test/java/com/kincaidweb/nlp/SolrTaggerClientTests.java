package com.kincaidweb.nlp;

import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertTrue;

public class SolrTaggerClientTests {


    @Test
    public void testQuery() {

        String text = "The patient has diabetes mellitus.";
        List<Map<String, Object>> results = SolrTaggerClient.solrTag(text);


        assertTrue(results.size() == 2);
    }
}

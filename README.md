# R package for sparklyr extensions. 
This project contains various sparklyr extensions and spark udf's.

## Installation
```R
devtools::install_github("IDEXX/idexx-data-science", subdir = "R-packages/sparklyr.idexx")
```

from inside R. You'll need to make sure you have a Github personal access token setup and have put
it in the environment variable `GITHUB_PAT` (best to do this in your `.Renviron` file in your home
directory. Go to https://github.com/settings/tokens to set this up. Be sure you give the PAT access
to the "repo".

## Usage
After connecting to spark using `sc <- spark_connect()` simple call `sparklyudf_register(sc)` to
register the UDF's in Spark and make them available.

## UDF's available
* **gunzip_string(bytes)**: takes one string argument of type raw (byte[]) that is a gzipped string
and returns the string value after decompression

* **striprtf(string)**: takes one string argument that is an RTF formatted document and returns the
text found within the document.
